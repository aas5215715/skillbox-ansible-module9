terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}



// Set variables
variable "yandex_token" {
    description = "Yandex Cloud security OAuth token"
    default     = "" #generate yours by this https://cloud.yandex.ru/docs/iam/concepts/authorization/oauth-token
    type = string
    sensitive = true
}
variable "yandex_cloud_id" {
    description = "Yandex Cloud ID where resources will be created"
    default     = ""
    type = string
    sensitive = true
}
variable "yandex_folder_id" {
    description = "Yandex Cloud Folder ID where resources will be created"
    default     = ""
    type = string
}



// Configure Yandex.Cloud provider
provider "yandex" {
  token     = var.yandex_token
  cloud_id  = var.yandex_cloud_id
  folder_id = var.yandex_folder_id
  zone      = "ru-central1-a"
}



// Create a new instance group
resource "yandex_compute_instance_group" "terr-vm-with-balancer" {
  name                = "terr-vm-with-balancer"
  folder_id           = var.yandex_folder_id
  service_account_id  = "ajevfbd23rm70tue0tj7"
  instance_template {
    platform_id = "standard-v2"
    resources {
      core_fraction = "20"
      memory = 1
      cores  = 2
    }
    boot_disk {
      initialize_params {
        image_id = "fd82sqrj4uk9j7vlki3q"
        type = "network-hdd"
        size = 15
      }
    }
    network_interface {
      network_id = "${yandex_vpc_network.vpc-network-lb.id}"
      subnet_ids = ["${yandex_vpc_subnet.vpc-subnet-lb.id}"]
      nat = true
    }
    metadata = {
      ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
    }
  }

  scale_policy {
    fixed_scale {
      size = 2
    }
  }

  allocation_policy {
    zones = ["ru-central1-a"]
  }

  deploy_policy {
    max_unavailable = 1
    max_expansion   = 0
  }

  load_balancer {
    target_group_name        = "target-group"
    target_group_description = "load balancer target group"
  }

}



// Configure load balancer
resource "yandex_lb_network_load_balancer" "load-balancer" {
  name = "load-balancer"

  listener {
    name = "load-balancer-listener"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = "${yandex_compute_instance_group.terr-vm-with-balancer.load_balancer.0.target_group_id}"
    healthcheck {
      name = "http"
      http_options {
        port = 80
        path = "/"
      }
    }
  }
}



// Configure VPC Nework
resource "yandex_vpc_network" "vpc-network-lb" {
  name = "vpc-network-lb"
}
resource "yandex_vpc_subnet" "vpc-subnet-lb" {
  name           = "vpc-subnet-lb"
  zone           = "ru-central1-a"
  network_id     = "${yandex_vpc_network.vpc-network-lb.id}"
  v4_cidr_blocks = ["10.240.1.0/24"]
}